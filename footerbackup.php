<?php
/**
 * @package Serene
 * @since Serene 1.0
 */
?>
		<footer id="main-footer">
			<?php get_sidebar( 'footer' ); ?>
			<p id="footer-info">
				<a href="http://wordpress.org/" rel="generator">Proudly powered by WordPress</a>
				<?php printf( __( 'Theme: %1$s by %2$s.', 'Serene' ), 'Serene', '<a href="http://www.elegantthemes.com/" rel="designer">Elegant Themes</a>' ); ?>
			</p>
		</footer> <!-- #main-footer -->
	</div> <!-- #container -->

	<?php wp_footer(); ?>

  	<!-- main nav -->
	<div class="main-nav">

		<ul>
			<li><a href="https://adtailor.com/home/index.jsp">Home</a></li>
			<li><a href="https://adtailor.com/home/howitworks.jsp">How it Works</a></li>
			<li><a href="https://adtailor.com/home/pricing.jsp">Pricing</a></li>
			<li><a href="https://adtailor.com/home/usecases.jsp">Use Cases</a></li>
			<li><a href="https://adtailor.com/demo/">Online Demo</a></li>
			<li><a href="https://adtailor.com/crm/">Login</a></li>
			<li><a href="" class="contactus-btn">Contact Us</a></li>
		</ul>

	</div>
</body>
</html>