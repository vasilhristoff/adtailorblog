<?php
/**
 * @package Serene
 * @since Serene 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?><?php echo serene_get_post_background(); ?>>

		<!-- change category signup text  -->
		<div style="display:none;" class="category-name">

			<?php 
				$categories = get_the_category();
				$category_id = $categories[0]->cat_name;
				echo $category_id; 
				switch($category_id) {
					case 'E-commerce' :
						$content = 'Sign Up For Our Monthly Newsletter! Learn more tips and tricks on how to increase profit and sales on your e-commerce site.';
						break;
					case 'Big Data' :
						$content = 'Sign Up For Our Monthly Newsletter! Stay up-to-date with news from the world of big data and see how you can use it to increase engagement and profit.' ;
						break;
					case 'Adtailor' :
						$content = 'Sign Up For Our Monthly Newsletter! Enjoy more of the Adtailor sweet stuff you love and learn new tricks on how to use the solution smarter. ';
						break;
					case 'Content Marketing' :
						$content = 'Sign Up For Our Monthly Newsletter! Learn what’s hot when it comes to content marketing and optimization, and apply it to your marketing strategy.';
						break; 
					case 'Events' : 
						$content = 'Sign Up For Our Monthly Newsletter! We’ll update you on the hottest events from the world of technology, marketing and personalization, and share our experience.';
						break;
					case 'Affilate Marketing' : 
						$content = 'Sign Up For Our Monthly Newsletter! Learn how to boost your affiliate marketing campaigns, grow your customer base, and respond to their needs and interests.';
						break;
					case 'Advertising' : 
						$content = 'Sign Up For Our Monthly Newsletter! Get relevant info on how to enhance ads and banners, personalize online experience, and get more CTR.';
						break;
					case 'Uncategorized' :
						$content = 'Sign Up For Our Monthly Newsletter! Enjoy more of the Adtailor sweet stuff you love.';
				}
			 ?>
		
		</div>	

		<p class="content-text"  style="display:none;font-size:28px;color:red">
			<?php echo $content;  ?>
		</p>

		<script>
		jQuery(document).ready( function() {
			if(jQuery('body').hasClass('single')) {
				var formText = jQuery(".content-text").text();
				jQuery(".es_caption").html(formText);
			}
		});
		</script>
	
		<!-- caption end change catrogry singup text  -->	
	
		<span class="page-view-container"><?php if(function_exists('the_views')) { the_views(); } ?></span>
		<?php if ( '' != get_the_post_thumbnail() || 'video' === get_post_format() ) : ?>

		<div class="main-image<?php if ( '' == get_the_post_thumbnail() && 'video' === get_post_format() ) echo ' et_video_only'; ?>">

			<?php if ( 'video' === get_post_format() && false !== ( $first_video = et_get_first_video() ) ) : ?>
			<div class="et-video-container">
				<?php echo $first_video; ?>
			</div>
			<?php endif; ?>

			<?php if ( ! is_single() || ! 'video' !== get_post_format() ) : ?>
			<a href="<?php the_permalink(); ?>" class="et-main-image-link">
			<?php endif; ?>

				<?php the_post_thumbnail( 'serene-featured-image' ); ?>

				<?php serene_post_meta_info(); ?>

			<?php if ( isset( $first_video ) && $first_video ) : ?>
				<span class="et-play-video"></span>
			<?php endif; ?>

			<?php if ( ! is_single() || ! 'video' !== get_post_format() ) : ?>
			</a>
			<?php endif; ?>

		</div> <!-- .main-image -->

		<?php endif; ?>

		<div class="post-content clearfix">
			<span style="color:#555;font-family:'Proxima-Light',sans-serif;font-size:14px;text-decoration:underline;display:inline-block;padding-right:15px;">Author:</span>
			<span style="color:#000;font-family:'Proxima-Bold',sans-serif;font-size:17px;text-transform:capitalize;display:inline-block;padding-right:15px;font-style:italic;"><?php the_author(); ?></span> 
		
			<?php if ( is_single() ) : ?>
			<h1 class="title"><?php the_title(); ?></h1>
			<?php else : ?>
			<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php endif; ?>

			<?php et_postinfo_meta(); ?>

			<div class="entry-content clearfix">
			<?php
			if ( is_single() ) {
				the_content();

				wp_link_pages( array(
					'before'         => '<p><strong>' . esc_attr__( 'Pages', 'Serene' ) . ':</strong> ',
					'after'          => '</p>',
					'next_or_number' => 'number',
				) );

				the_tags( '<ul class="et-tags clearfix"><li>', '</li><li>', '</li><li style="background:none;display:block;"><a href="https://adtailor.com" style="font-size:1.1em;background:#7db6f5;border-radius:500px;text-transform:capitalize;font-size:1.3em;padding:.2em 1.5em"><i class="ion-link" style="font-size:1.5em;margin-right:10px;position:relative;top:3px;"></i>Try Adtailor for free</a></li></ul>' );
				edit_post_link( esc_attr__( 'Edit this post', 'Serene' ) );
			} else {
				the_excerpt();?>
<div class="" style="text-align:center;padding-top:.6em;"><a href="<?php the_permalink();  ?>" style="display: inline-block;background: #7db6f5;color: #fff;padding: .3em 2em;border-radius: 5px;">Read more</a></div>
			<?php
			}
			?>


			</div>
	</div> <!-- .post-content -->

		<?php
		if ( '' == get_the_post_thumbnail() )
			serene_post_meta_info();
		?>
	</article>

